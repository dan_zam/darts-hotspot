%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Darts' Hotspot                                   %
% -------------------------------------------------%
% author: Daniele Zambon                           %
% update: 21/06/2015                               %
% -------------------------------------------------%
%                                                  %
% This is a tool to visualize which is the point   %
% one has to aim in order to maximize his score.   %
% For short, it makes a convolution of the the     %
% scores function (2D image) with a kernel that    %
% represents the distribution of the player's      %
% shots.                                           %
%                                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

files:
    target.png
        grayscale representation of the scores.
        you can use one made by yourself, just
        remember to leave a zero-scored area
        around the target.
        
    scores.png
        assigns the score at each section. Remind
        that the inner circle triplicate the
        score and the outer circle duplicate.
    
    moll025.png
        gives an example of the output. The red
        region is the one with higher estimated
        score expectation.
             
    example.png
        is the an edited output that focus on
        the precice location on the target to which
        aim your shots.

    main.m
        Octave main script where you have to set
        your parameters.
        
    kernelgen.m
        Kernel Generator is a Octave function that 
        generates the kernel to be used for the
        convolution. It represent the skill of the
        player, namely the distribution of its
        shots.
        
    hotspot.c hotspot.o hotspot.mex
        it does the convolution between the score
        function and the kernel.
