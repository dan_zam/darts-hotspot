%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Darts' Hotspot                                   %
% -------------------------------------------------%
% author: Daniele Zambon                           %
% update: 21/06/2015                               %
% -------------------------------------------------%
%                                                  %
% This is a tool to visualize which is the point   %
% to aim in order to maximize your score.          %
% For short it makes a convolution of the the      %
% scores function (2D image) with a kernel         %
% (representing the distribution of the player's   %
% shots).                                          %
%                                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
function k=kernelgen(targetdiam,ratio,kerneltype)
    n=floor(targetdiam*ratio);
    if (mod(n,2)==0)
        n=n+1;
    end
    switch(kerneltype)
        case 1
            k=uniform(n);
        case 2
            k=mollifier(n);
        case 3
            k=gaussian(n);
        otherwise
            k=uniform(n);
    endswitch
end
    
function k=uniform(n)
    k=zeros(n,n);
    c=n*.5;
    for i=1.:n
        for j=1.:n
            if(norm([i-c,j-c])<=n*.5)
                k(i,j)=1.;
            end
        end
    end
    k=k./(sum(sum(k)));
end

function k=mollifier(n)
    k=zeros(n,n);
    c=(n+1)*.5;
    for i=1.:n
        for j=1.:n
            r2=(i-c)*(i-c)+(j-c)*(j-c);
            r2=r2/n/n*4;
            if (r2<=1)
                k(i,j)=exp(1/(r2-1));
            end
        end
    end
    k=k./sum(sum(k));
end

function k=gaussian(n)
    n=n;
    k=zeros(n,n);
    c=n*.5;
    for i=1.:n
        for j=1.:n
            r2=(i-c)*(i-c)+(j-c)*(j-c);
            k(i,j)=exp(-r2/n/n*4);
        end
    end
    k=k./sum(sum(k));
end
