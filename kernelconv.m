%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Darts' Hotspot                                   %
% -------------------------------------------------%
% author: Daniele Zambon                           %
% update: 21/06/2015                               %
% -------------------------------------------------%
%                                                  %
% This is a tool to visualize which is the point   %
% to aim in order to maximize your score.          %
% For short it makes a convolution of the the      %
% scores function (2D image) with a kernel         %
% (representing the distribution of the player's   %
% shots).                                          %
%                                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
function [ass,rel]=kernelconv(img,k)

im=img(:,:,1)./3.+img(:,:,2)./3.+img(:,:,3)./3.;
im=double(im);

[n,m]=size(im);

%kernel dim must be odd
[kn,km]=size(k);

x=255*ones(n+kn-mod(kn,2),m+km-mod(km,2));
y=zeros(n,m);

xn=[floor(kn/2.)+1,n+floor(kn/2.)];
xm=[floor(km/2.)+1,m+floor(km/2.)];

x(xn(1):xn(2),xm(1):xm(2))=im;

for i=1:n
    for j=1:m
        tmp=x(i:i+kn-1,j:j+km-1).*k;
        y(i,j)=sum(sum(tmp));
    end
end

for i=1:3 ass(:,:,i)=uint8(y); end
y=y-min(min(y));
y=y/max(max(y))*255;
for i=1:3 rel(:,:,i)=uint8(y); end
index=rel<min(min(rel))+10;

rel(index(:,:,2))+=255;

end
