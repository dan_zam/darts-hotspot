%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Darts' Hotspot                                   %
% -------------------------------------------------%
% author: Daniele Zambon                           %
% update: 21/06/2015                               %
% -------------------------------------------------%
%                                                  %
% This is a tool to visualize which is the point   %
% to aim in order to maximize your score.          %
% For short it makes a convolution of the the      %
% scores function (2D image) with a kernel         %
% (representing the distribution of the player's   %
% shots).                                          %
%                                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
clear all,close all

% just set the three following parameters
ratio=0.5; %ratio between kernel "diameter" 
            %and target diameter
kerneltype=2;   %1 uniform (ball radius=target*ratio)
                %2 canonical mollifier (support ball 
                %                radius=target*ratio)
                %3 gaussian (std dev=target*ratio)
filename="target.png"; %filename of the target
                       %scores image


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% don't edit the following code                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%load the image
img=imread(filename);

%generate the kernel
[n,~,~]=size(img);
k=kernelgen(n,ratio,kerneltype);

%evaluate the convolution
im=double(img(:,:,1));
tic
[ass,rel,marker]=hotspot(im,k);
for i=1:3 y(:,:,i)=uint8(ass); end
for i=1:2 z(:,:,i)=uint8(rel); end
z(:,:,3)=uint8(marker);
toc

%plot the results
subplot(2,2,1), imshow(img)
img=double(img);
img=img-min(min(img));
img=img./max(max(img))*255.;
img=uint8(img);
subplot(2,2,2), imshow(img)
subplot(2,2,3), imshow(y)
subplot(2,2,4), imshow(z)
