/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Darts' Hotspot                                   %
% -------------------------------------------------%
% author: Daniele Zambon                           %
% update: 23/06/2015                               %
% -------------------------------------------------%
%                                                  %
% This is a tool to visualize which is the point   %
% to aim in order to maximize your score.          %
% For short it makes a convolution of the the      %
% scores function (2D image) with a kernel         %
% (representing the distribution of the player's   %
% shots).                                          %
%                                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
#include "mex.h"
#include <math.h>

//prototypes
double kernelsum(double*,int,int,double*,int,int,int,int);
void kernelconv(double*,double*,double*,double*,double*,int,int,int,int,double*);


void mexFunction(int nlhs, mxArray *plhs[], int nrhs,
                 const mxArray *prhs[]){

    double *im,*k,*ass,*rel,*marker,*tol;
    int m, n, km, kn;

    //Check for proper number of arguments.
    if (nrhs != 2 && nrhs != 3)
        mexErrMsgTxt("Two inputs required (plus one optional).");
    
    //The inputs must be a noncomplex scalar double.
    if (!mxIsDouble(prhs[0]) || mxIsComplex(prhs[0]))
        mexErrMsgTxt("Input 1 must be a noncomplex matrix of double.");
    if (!mxIsDouble(prhs[1]) || mxIsComplex(prhs[1]))
        mexErrMsgTxt("Input 2 must be a noncomplex matrix of double.");

    //input dimesions
    m = mxGetM(prhs[0]);
    n = mxGetN(prhs[0]);
    km = mxGetM(prhs[1]);
    kn = mxGetN(prhs[1]);

    //Create matrix for the return argument
    plhs[0] = mxCreateDoubleMatrix(m,n, mxREAL); //ass
    plhs[1] = mxCreateDoubleMatrix(m,n, mxREAL); //rel
    plhs[2] = mxCreateDoubleMatrix(m,n, mxREAL); //marker
        
    //Assign pointers to each input and output
    im = mxGetPr(prhs[0]);
    k = mxGetPr(prhs[1]);
    ass = mxGetPr(plhs[0]);
    rel = mxGetPr(plhs[1]);
    marker = mxGetPr(plhs[2]);
    if (nrhs == 2){
        tol=malloc(sizeof(double));
        tol[0]=10;
    }else
        tol=mxGetPr(prhs[2]);
        
    //let's do THE thing :)
    kernelconv(ass,rel,marker,im,k,n,m,kn,km,tol);
}


void kernelconv(double *ass,double *rel, double *marker,double *im,
                double *k, int m,int n, int km,int kn, double *tol){

    //temporary array
    double x[(m+km-km%2)*(n+kn-kn%2)];

    //init x (=temporary matrix with im infos)
    int i, j;
    for (i=0;i<m+km-km%2;i++)
        for (j=0;j<n+kn-kn%2;j++)
            x[j+i*(n+kn-kn%2)]=255.;

    //copy im to the center of x
    int ix, jx;
    for (i=0;i<m;i++){
        ix=floor(km/2.)+i;
        for (j=0;j<n;j++){
            jx=floor(kn/2.)+j;
            x[jx+ix*(n+kn-kn%2)]=im[j+i*n];
        }
    }

    //convolution
    double max=0, min=255;
    for (i=0;i<m;i++)
        for (j=0;j<n;j++){
            ass[j+i*n]=kernelsum(x,m,n,k,km,kn,i,j);
            if(ass[j+i*n]>max)
                max=ass[j+i*n];
            else if(ass[j+i*n]<min)
                min=ass[j+i*n];
        }
  
    //cooler output
    for (i=0;i<m;i++)
        for (j=0;j<n;j++){
            rel[j+i*n]=ass[j+i*n]-min;
            rel[j+i*n]=rel[j+i*n]/(max-min)*255;
            if (rel[j+i*n]<tol[0])
                marker[j+i*n]=255.;
            else
                marker[j+i*n]=rel[j+i*n];  
        }
}


double kernelsum(double *x,int m,int n,double *k,int km,int kn,int i,int j){
    int ii, jj;
    double sum=0;
    //Convolution
    for (ii=0;ii<km;ii++){
        for (jj=0;jj<kn;jj++){
            sum+=x[(jj+j)+(ii+i)*(n+kn-kn%2)]*k[jj+ii*kn];
        }
    }
    return sum;
}
